<%@ page import="org.apache.axiom.soap.SOAPBody" %>
<%@ page import="org.springframework.context.ApplicationContext" %>
<%@ page import="org.springframework.context.support.ClassPathXmlApplicationContext" %>
<%@ page import="org.springframework.ws.soap.axiom.AxiomMtomClient"%>
<%@ page import="com.endeca.navigation.*" %>
<%@ page import="com.endeca.ui.*" %>
<%@ page import="com.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="com.endeca.ui.constants.*" %>
<%@ page import="com.plm.constants.PLMConstants" %>
<%
	String fileName = request.getParameter("fileName");
	response.setContentType("application/zip");
	response.setHeader("Content-disposition", "attachment;filename="+fileName); // To pop dialog box

	String downloadFileOutputpath =UI_Props.getInstance().getValue(PLMConstants.DEFAULT_DATA_PHOTO_DOWNLOAD_PATH_LABEL);
	File f = new File (downloadFileOutputpath+"/"+fileName);
	//OPen an input stream to the file and post the file contents thru the
	//servlet output stream to the client m/c
	InputStream in = new FileInputStream(f);
	int bit = 256;
	int i = 0;
	out.clearBuffer();
	try {
		while ((bit) >= 0) {
			bit = in.read();
			out.write(bit);
		}
	} catch (IOException ioe) {
		ioe.printStackTrace(System.out);
	}
	in.close();
	out.flush();
%>