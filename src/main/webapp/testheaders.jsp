<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <HEAD>
        <TITLE>Reading Header Information</TITLE>
    </HEAD>
    <script LANGUAGE="JavaScript">
	function resetPassword() {
		var loginid = '<%=request.getHeader("USERID")%>';
		var hostname= '<%=request.getHeader("Host")%>';
		var requestSecure = '<%=request.isSecure()%>';
		var connType = (requestSecure)? "https://" : "http://";
		var redirectURL = location.href;
		var resetPasswdURL = connType+hostname+"/identity/oblix/apps/lost_pwd_mgmt/bin/lost_pwd_mgmt.cgi?program=redirectforchangepwd&"+
		"login="+loginid+"&backURL="+redirectURL+"&target=top";
		//alert(" URL : " + resetPasswdURL);
		location.href=resetPasswdURL;
	}
	
	function logoff() {
		var hostname= '<%=request.getHeader("Host")%>';
		var requestSecure = '<%=request.isSecure()%>';
		var connType = (requestSecure)? "https://" : "http://";
		var logoffURL = connType+hostname+"/plmweb/logout.htm";
		//alert(" URL : " + logoffURL );
		location.href=logoffURL;
	}

	</script>	
    <BODY>
	  <br><a href="#" onclick="javascript:resetPassword()">Reset Password</a>
	  <br><a href="#" onclick="javascript:logoff()">Logoff</a>
	  <br> 		
        <H1>Reading Header Information</H1>
        Here are the request headers and their data:
        <BR>
	  <TABLE border="1">
	  <% java.util.Enumeration names = request.getHeaderNames();
        while(names.hasMoreElements()){
            String name = (String) names.nextElement();
	  %>
		<TR>
	  <%		
            out.println("<TD>"+ name + ":</TD><TD>" + request.getHeader(name) + "</TD>");
	  %>
		</TR>
	  <%		
        }
        %>
	  </TABLE>
	  <BR></BR>	
	   <TABLE border="1">
	  <% names = request.getAttributeNames();
        while(names.hasMoreElements()){
            String name = (String) names.nextElement();
	  %>
		<TR>
	  <%		
            out.println("<TD>"+ name + ":</TD><TD>" + request.getAttribute(name) + "</TD>");
	  %>
		</TR>
	  <%		
        }
        %>
	  </TABLE>				
	  <BR></BR>	
	   <TABLE border="1">
	  <% 
	  Cookie cookies [] = request.getCookies ();
	  Cookie myCookie = null;

	  for (int i = 0; i < cookies.length; i++) {
		  myCookie = cookies[i];
	  %>
		<TR>
	  <%		
            out.println("<TD>Name : </TD><TD>" + myCookie.getName() + "</TD>");
	  %>
		</TR>
		<TR>
	  <%		
            out.println("<TD>&nbsp;&nbsp;&nbsp;Value : </TD><TD>" + myCookie.getValue() + "</TD>");
	  %>
		</TR>
		<TR>
	  <%		
            out.println("<TD>&nbsp;&nbsp;&nbsp;MaxAge : </TD><TD>" + myCookie.getMaxAge() + "</TD>");
	  %>
		</TR>
		<TR>
	  <%		
            out.println("<TD>&nbsp;&nbsp;&nbsp;Path : </TD><TD>" + myCookie.getPath() + "</TD>");
	  %>
		</TR>
		<TR>
	  <%		
            out.println("<TD>&nbsp;&nbsp;&nbsp;Domain : </TD><TD>" + myCookie.getDomain() + "</TD>");
	  %>
		</TR>
		<TR>
	  <%		
            out.println("<TD>&nbsp;&nbsp;&nbsp;Comment : </TD><TD>" + myCookie.getComment() + "</TD>");
	  %>
		</TR>
		<TR>
	  <%		
            out.println("<TD>&nbsp;&nbsp;&nbsp;Secure : </TD><TD>" + myCookie.getSecure() + "</TD>");
	  %>
		</TR>
	  <%		
        }
        %>
	  </TABLE>				
    </BODY>
</html>