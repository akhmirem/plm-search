package com.plm.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.plm.util.AccessControlManager;
import com.plm.util.PermissionInfo;




public class OlatRedirectServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7483019740473168673L;
	private ServletContext servletContext;
	
	public void init(ServletConfig servletConfig) throws ServletException {
         super.init(servletConfig);
         servletContext = servletConfig.getServletContext();
	}	

	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	throws IOException, ServletException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		AccessControlManager acm = new AccessControlManager(servletContext);
		HttpSession httpSession = request.getSession();
		String firstName = null;
		String lastName = null;
		String email = null;
		String groupName = null;
		String userId = null;
		String ipAddress = null;
		//Added by Ashish to get SSO Params from headers
		userId = request.getHeader("USERID");
		if(userId == null){
			userId = request.getParameter("user_id");
		}
		groupName = request.getHeader("GROUPS");
		if(groupName == null) {
			groupName = request.getParameter("group");
		}
		firstName = request.getHeader("FIRSTNM") != null ? request.getHeader("FIRSTNM") : userId;
		lastName = request.getHeader("LASTNM") != null ? request.getHeader("LASTNM") : "";
		email = request.getHeader("MAIL") != null ? request.getHeader("MAIL") : "";
		
		String fromCalParole = (String) request.getAttribute("fromCP");
		
		if("Y".equals(fromCalParole)){
			userId = (String) request.getAttribute("USERID");
			groupName = "LEADS-MapUsers:LEADS-Query:LEADS-Downloaders";
			firstName = (String) request.getAttribute("FIRSTNM");
			lastName = (String) request.getAttribute("LASTNM");
			email = (String) request.getAttribute("MAIL");
			ipAddress = (String) request.getAttribute("IPADDRESS");
		}
		
		PermissionInfo permissionInfo = null;
		try {
			if(groupName == null){
				groupName = "LEADS-Query"; //Default Group
			}
			permissionInfo = acm.getPermission(groupName);
			httpSession.setAttribute("permissionInfo", permissionInfo);
		}catch(Exception e) {
			e.printStackTrace();
		}

		request.setAttribute("userName", userId);
		request.setAttribute("firstName", firstName);
		request.setAttribute("lastName", lastName);
		request.setAttribute("email", email);
		request.setAttribute("groupName", groupName);
		//request.setAttribute("userId", userId);
		String userType = permissionInfo.getUserType();
		request.setAttribute("userType",userType);
		request.setAttribute("fromLoginPage","Y");
		request.setAttribute("ipAddress",ipAddress);
		
		//Set the properties in session instead of request
		httpSession.setAttribute("userId",userId);
		httpSession.setAttribute("firstName",firstName);
		httpSession.setAttribute("lastName",lastName);
		httpSession.setAttribute("email",email);
		httpSession.setAttribute("groupName",groupName);
		httpSession.setAttribute("userType",userType);
		httpSession.setAttribute("ipAddress",ipAddress);

		if(httpSession.getAttribute("isFirstLogin")== null){
			httpSession.setAttribute("fromLoginPage","Y");	
		}
		httpSession.setAttribute("isFirstLogin","Y");

		RequestDispatcher dispatcher = null;
		if("Y".equals(fromCalParole)){
			request.setAttribute("fromCP", "Y");
			httpSession.setAttribute("fromCP", "Y");
			dispatcher = request.getRequestDispatcher("goToCPMainPage");
		} else {
			dispatcher = request
			.getRequestDispatcher("goToPLMMainPage");
		}		
		
		/*RequestDispatcher dispatcher = request
				.getRequestDispatcher("goToPLMMainPage");*/
		dispatcher.forward(request, response);
	}
}