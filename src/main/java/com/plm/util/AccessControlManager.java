package com.plm.util;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.endeca.ui.export.PDFExportServlet;

/**
 * Access Control Manager which provides the Permission / User Roles.
 */
public class AccessControlManager {

	public static final String NODE_ROLE_MANAGER="ROLE-MANAGER";
	public static final String NODE_GROUP="GROUP";
	
	public static final String NODE_DOWNLOAD="DOWNLOAD";
	public static final String NODE_QUERY="QUERY";
	public static final String NODE_USER="USER";
	public static final String NODE_VIEWMAPS="VIEWMAPS";
	public static final String NODE_AMS="AMS";

	public static final String ATTR_DEFAULT="default";
	public static final String ATTR_VIEW="view";
	public static final String ATTR_TYPE="type";
	
	public static final String ADMIN="admin";
	public static final String AUTHOR="author";
	public static final String USER="user";
	public static final String GUEST="guest";
	
	private static final String separator = ":";
	private static Document doc = null;

	private ServletContext servletContext;
	private static Hashtable<String, PermissionInfo> htPermission = null;
	private static PermissionInfo defaultPermission = null;
	private static final Log logger = LogFactory.getLog(AccessControlManager.class);

	public AccessControlManager(ServletContext sc) {
		servletContext = sc;		
		if(htPermission == null || htPermission.size() == 0 ) {
			htPermission = new Hashtable<String, PermissionInfo>();			
			try {
				doc = getConfigDocument(servletContext);
			}catch(Exception e) {
				e.printStackTrace();
			}
			init();
		}
	}
	
	/**
	 * Initialize the UserType and Permissions from roleConfig.xml file.
	 */
	private void init() {
		if( doc != null ) {
			processGroups(doc.getChildNodes());
		}
	}
	
	/**
	 * This extract the Group details from the XML DOM and creates the cache of PermissionInfo objects.
	 * @param nodeList of Root node.
	 */
	private void processGroups(NodeList nodeList) {
		if(nodeList != null) {
			int nodeListSize = nodeList.getLength();
			for(int i=0;i < nodeListSize; i++) {
				Node node = nodeList.item(i);
				String nodeName = node.getNodeName();
				if(nodeName.equalsIgnoreCase(NODE_GROUP) ) {
					String userGroupName = node.getAttributes().getNamedItem("name").getNodeValue();
					String groupType = null;
					if( node.getAttributes().getNamedItem(ATTR_TYPE) != null ) {
						groupType = node.getAttributes().getNamedItem(ATTR_TYPE).getNodeValue();
					}
					PermissionInfo pInfo = new PermissionInfo();
					NodeList groupPermissionList = node.getChildNodes();
					int nlSize = groupPermissionList.getLength();
					for(int j=0;j < nlSize; j++) {
						Node permissionNode = groupPermissionList.item(j);
						String permissionNodeName = permissionNode.getNodeName();
						if(permissionNodeName.equalsIgnoreCase(NODE_DOWNLOAD) ) {
							NamedNodeMap nnmAttributes = permissionNode.getAttributes();
							Node viewAttrNode = nnmAttributes.getNamedItem(ATTR_VIEW);
							if(viewAttrNode.getNodeValue().trim().equalsIgnoreCase("true")) {
								pInfo.setCanDownload(true);
							}
						}else if(permissionNodeName.equalsIgnoreCase(NODE_QUERY) ) {
							NamedNodeMap nnmAttributes = permissionNode.getAttributes();
							Node viewAttrNode = nnmAttributes.getNamedItem(ATTR_VIEW);
							if(viewAttrNode.getNodeValue().trim().equalsIgnoreCase("true")) {
								pInfo.setCanQuery(true);
							}
						}else if(permissionNodeName.equalsIgnoreCase(NODE_VIEWMAPS) ) {
							NamedNodeMap nnmAttributes = permissionNode.getAttributes();
							Node viewAttrNode = nnmAttributes.getNamedItem(ATTR_VIEW);
							if(viewAttrNode.getNodeValue().trim().equalsIgnoreCase("true")) {
								pInfo.setCanViewMaps(true);
							}
						}else if(permissionNodeName.equalsIgnoreCase(NODE_USER) ) {
							NamedNodeMap nnmAttributes = permissionNode.getAttributes();
							Node viewAttrNode = nnmAttributes.getNamedItem(ATTR_TYPE);
							pInfo.setUserType(viewAttrNode.getNodeValue());
						}
						else if(permissionNodeName.equalsIgnoreCase(NODE_AMS) ) {
							NamedNodeMap nnmAttributes = permissionNode.getAttributes();
							Node viewAttrNode = nnmAttributes.getNamedItem(ATTR_VIEW);
							if(viewAttrNode.getNodeValue().trim().equalsIgnoreCase("true")) {
								pInfo.setCanViewAMS(true);
							}
						}
					}
					if(groupType != null && groupType.equalsIgnoreCase(ATTR_DEFAULT)) {
						defaultPermission = pInfo;
					}else{
						htPermission.put(userGroupName, pInfo);
					}
				}else{
					NodeList childNodes = node.getChildNodes();
					if(childNodes != null) {
						processGroups(childNodes);
					}
				}
			}
		}
	}
	
	/**
	 * This returns the PermissionInfo Object for the given UserGroup. It supports the multiple UserGroups separated by : sign.
	 * In case of multiple group it makes the UNION of access permission and Highest accessibility ROLE among the given UserGroups.
	 * @param roleName
	 * @return
	 */
	public PermissionInfo getPermission(String userGroup) {
		PermissionInfo retPInfo = null;
		String[] strGroups = userGroup.split(separator);
		if(strGroups != null && strGroups.length == 1) {
			retPInfo = htPermission.get(strGroups[0]);
		}else{
			for(int i=0; i<strGroups.length; i++) {
				PermissionInfo pInfo = htPermission.get(strGroups[i]);
				if(retPInfo == null && pInfo != null) {
					try {
						retPInfo = (PermissionInfo)pInfo.clone();
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
				if(retPInfo!= null && pInfo != null) {
					if( pInfo.canDownload() ) {
						retPInfo.setCanDownload(pInfo.canDownload());
					}
					if( pInfo.canQuery() ) {
						retPInfo.setCanQuery(pInfo.canQuery());
					}
					if( pInfo.canViewMaps() ) {
						retPInfo.setCanViewMaps(pInfo.canViewMaps());
					}
					if( pInfo.canViewAMS() ) {
						retPInfo.setCanViewAMS(pInfo.canViewAMS());
					}
					//If returning Object has the Lower User type then the comparing Object - then upgrade the User Type to higher user type.
					if( retPInfo.isGuest() && !pInfo.isGuest() ) {
						retPInfo.setUserType(pInfo.getUserType());
					}else if( retPInfo.isUser() && !pInfo.isGuest() && !pInfo.isUser() ) {
						retPInfo.setUserType(pInfo.getUserType());
					}else if( retPInfo.isAuthor() && !pInfo.isGuest() && !pInfo.isUser() && !pInfo.isAuthor() ) {
						retPInfo.setUserType(pInfo.getUserType());
					}
				}
			}
		}
		if(retPInfo == null) {			
			retPInfo = defaultPermission;
		}
		return retPInfo;
	}
	
	/**
	 * Implemented the methods for OLAT - to retain the existing functionality.
	 * @param sc - ServletContext.
	 * @return Map of UserType
	 */
	public Map getRolesMap(ServletContext sc) {
		return getRolesFor("LEADS-Downloaders:LEADS-Query");
	}	
	
	/**
	 * Implemented the methods for OLAT - to retain the existing functionality.
	 * @param roleName - user role name.
	 * @return Map of UserTypes
	 */
	public Map getRolesFor(String roleName) {
		HashMap<String, Object> roleMap = new HashMap<String, Object>();
		String[] strGroups = roleName.split(separator);
		for(int i=0; i<strGroups.length; i++) {
			PermissionInfo pInfo = htPermission.get(strGroups[i]);
			if(pInfo != null) {
				if( pInfo.isAdmin() ) {
					roleMap.put("isAdmin", pInfo.isAdmin());
				}else if( pInfo.isAuthor() ) {
					roleMap.put("isAuthor", pInfo.isAuthor());
				}else if( pInfo.isUser() ) {
					roleMap.put("isUser", pInfo.isUser());
				}else{
					roleMap.put("isGuest", true);
				}
			}
		}
		return roleMap;
	}	

	/**
	 * This reads the roleConfig.xml file and creates the Document object and returns it.
	 * @param sc - ServletContext
	 * @return - Document object.
	 */
	private Document getConfigDocument(ServletContext sc) {
		try {
			String fileName = sc.getRealPath("/WEB-INF/roleConfig.xml");
			if(fileName == null) {
				 URL resourceURL = sc.getResource("/WEB-INF/roleConfig.xml");
				 if(resourceURL != null) {
					 fileName = resourceURL.getPath();
				 }else{
					 logger.error("RoleConfig - ResourceURL is Null.." );
				 }
			}			
			File file = new File(fileName);
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			doc = docBuilder.parse(file);		
		}catch(Exception e) {
			e.printStackTrace();
		}
		return doc;
	}

	public static void main(String args[]) {
		AccessControlManager acm = new AccessControlManager(null);
		acm.init();
		
	}
}
