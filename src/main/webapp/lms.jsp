
<%@ page import="com.endeca.navigation.*" %>
<%@ page import="com.endeca.ui.*" %>
<%@ page import="com.util.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.endeca.ui.constants.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Parole LEADS 2.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<!-- demo js -->
<script language="JavaScript" src="javascript/prototype.js"></script>
<script language="JavaScript" src="javascript/endeca_util.js"></script>
<link href="media/style/thickbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="media/style/main.css" type="text/css"/>
<script type="text/javascript" src="media/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="media/js/thickbox.js"></script>
<script type="text/javascript" src="media/js/common.js"></script>
</head>
<body>
<div align="center">
<div id="Maindiv">
<div id="Container" >
  <div id="Header">
  <div id="logoimg">
  <a href="welcome_screen_rd.html"><img src="media/images/global/logo.gif" alt="CDCR" border="0"/></a>  </div>
    <div id="topglobalnav">
	<!-- Header Part Start -->
		<%@ include file="header.jsp" %>
	<!-- Start Global Nav Table -->
		<%@ include file="global_nav.jsp" %>
	<!--End Global Nav Table -->
	</div>		
  </div>
<!-- Header Part End -->   
<!-- Middle Part Start --> 
  <div id="Middle"  class="content_box" align="center">
	<img src="media/images/global/under_contruction.jpg" alt="CDCR" border="0"/>
  </div>
<div id="Footer" >
  
</div>
<script language="JavaScript">
$('#welcomett_nav').attr('class', 'selected');
</script>
<!-- Footer Part End -->
	</div>
  </div>
</div>
</div>
</div>
</div>
</body></html>