<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ page isErrorPage="true" %>
<%@ page import="com.plm.util.PLMSearchUtil" %>
<%@ page import="java.io.*" %>
<%@ page import="org.apache.commons.logging.Log" %>
<%@ page import="org.apache.commons.logging.LogFactory" %>
<%@ page import="java.net.URLDecoder" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Exception occurred</title>
	</head>
	<body>
		<div style="float:left; height:450px; padding-left:10px">
			<pre><H3>Parole LEADS 2.0 Search is currently not available. Please try again later or contact your Help Desk co-ordinator.</H3></pre>
<%
			final Log logger = LogFactory.getLog("Search_Error");
			String tempQueryString = URLDecoder.decode(request.getQueryString(),"UTF-8");
			String queryString = PLMSearchUtil.encodeGeoCodeCriteria(tempQueryString);

			if(exception != null){ 
			 	StringWriter sw = new StringWriter();
			 	exception.printStackTrace(new PrintWriter(sw));
			 	StringBuffer sbTrace = new StringBuffer();
			 	sbTrace.append("\n");
			 	sbTrace.append("LoginId		:" + session.getAttribute("userId") + ", Full Name		:" + session.getAttribute("firstName") + " " + session.getAttribute("lastName"));
			 	sbTrace.append("\n");
			 	sbTrace.append("Group		:" + session.getAttribute("groupName") + ", UserType 	:" + session.getAttribute("userType"));
			 	sbTrace.append("\n");
			 	sbTrace.append("Request		:" + queryString);
			 	sbTrace.append("\n");
			 	sbTrace.append("Exception	:"+ sw.toString());
			 	logger.error(sbTrace);
			}

%>
		</div>
	</body>
</html>
