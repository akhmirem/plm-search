package com.plm.servlets;

import java.io.IOException;

import javax.servlet.GenericServlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.quartz.CronExpression;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.scheduling.quartz.CronTriggerBean;

import com.plm.oam.apps.LDAPInactiveUsersCronJob;

public class SchedulerServlet extends GenericServlet {

	/**
	 * 
	 * Constant to represent property for the cron expression.
	 */

	private static final String CRON_EXPRESSION = "0 0 20 ? * * ";

	public void init(ServletConfig servletConfig) throws ServletException {

		super.init(servletConfig);

		// The Quartz Scheduler

		Scheduler scheduler = null;

		try {

			// Initiate a Schedule Factory

			SchedulerFactory schedulerFactory = new StdSchedulerFactory();

			// Retrieve a scheduler from schedule factory

			scheduler = schedulerFactory.getScheduler();

			// Initiate JobDetail with job name, job group and

			// executable job class

			JobDetail jobDetail = new JobDetail("RetryJob", "RetryGroup",
					LDAPInactiveUsersCronJob.class);

			// Initiate CronTrigger with its name and group name

			CronTrigger cronTrigger = new CronTrigger("cronTrigger",
					"triggerGroup");

			// setup CronExpression

			CronExpression cexp = new CronExpression(CRON_EXPRESSION);
			
			//CronTriggerBean crontrigger = new CronTriggerBean();
			

			// Assign the CronExpression to CronTrigger

			cronTrigger.setCronExpression(cexp);

			// schedule a job with JobDetail and Trigger

			scheduler.scheduleJob(jobDetail, cronTrigger);

			// start the scheduler
			scheduler.start();
			
			

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	public void service(ServletRequest serveletRequest,
			ServletResponse servletResponse) throws ServletException, IOException {

	}

}
