<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Parole LEADS 2.0 Terms and Conditions</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<link rel="stylesheet" href="media/style/main_welcome.css" type="text/css"/>
		<link href="media/style/thickbox.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="media/js/jquery-1.3.2.js"></script>
		<script type="text/javascript" src="media/js/thickbox.js"></script>
	<script>
		function closeWindow() {
			self.close();
		}
	</script>
	</head>
	<body>
		<div align="center">
			<form name="tc" id="tc" action="">
				<div align="center">
					<div id="Maindiv">
						<div id="Container">
							<div id="Header">
								<div id="logoimgTerms">
										<img border="0" alt="CDCR" src="media/images/global/logo.gif"/>
								</div>
								<div id="topglobalnavTerms">
									<div id="Top">
										<div>
											<div class="headerNav">
												<div id="paroleesearch_text"><img src="media/images/global/paroleesearch_text.gif" alt="" /></div>
											</div>
										</div>
									</div>
									<div id="GlobalNav" class="blankglobalnav">
										<img src="media/images/global/spacer.gif" />
									</div>
								</div>
							</div>
							<div id="Middle"  class="content_box">
								<div  id="middletop">
									<jsp:include page="terms.html" flush="true" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>