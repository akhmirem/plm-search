<%@ page import="org.apache.commons.logging.Log" %>
<%@ page import="org.apache.commons.logging.LogFactory" %>
<%-- <%@ page import="com.endeca.ui.constants.*" %>
<%@page import="com.plm.constants.PLMConstants"%>
<%@ page import="com.plm.oam.utils.OLATUtil" %>  
<%@ page import="com.plm.oam.apps.LDAPModifyUserAttribute" %>
 --%>
<%
	final Log logger = LogFactory.getLog(this.getClass());
/* 	
	String isProsLeadsAccountAdmin = request.getHeader("PROSLEADSADM");
	String adminTestName = UI_Props.getInstance().getValue("ADMIN_TEST_NAME");	
	String userId = (String) session.getAttribute("userId");
	if (userId == null) {
		userId = (String) request.getAttribute("userName");
	}
	
	logger.debug("isProsLeadsAccountAdmin :: " + isProsLeadsAccountAdmin);
	logger.debug("adminTestName :: " + adminTestName);
	logger.debug("userId :: " + userId);
	if(isProsLeadsAccountAdmin != null && isProsLeadsAccountAdmin.equals("true")){
		 int userPassedTest = OLATUtil.getTestResultsForUser(userId, adminTestName);
		 if(userPassedTest == 1){
			LDAPModifyUserAttribute.moveUser(userId, PLMConstants.USER_GROUP, PLMConstants.NEW_GROUP);
			LDAPModifyUserAttribute.updateAttribute(userId,PLMConstants.USER_IS_A_PROSPECTIVE_LEADS_ACCOUNT_ADMIN, PLMConstants.USER_MODIFY_VALUE);
		 }
	} 

 */	
 	String userId = (String)session.getAttribute("userId");
	String redirectURL = "/plmweb/logout.htm";
	if(userId == null){
		redirectURL = "plmredirect";
	}
	String fromCP = (String) session.getAttribute("fromCP");
	if(fromCP == null)
		fromCP = "N";
	session.invalidate();
%>
<script type="text/javascript">
	var jFromCP = "<%=fromCP%>";
	if(jFromCP == "Y"){
		self.close();
	}else{
		top.location.href='<%=redirectURL%>';
	}
</script>